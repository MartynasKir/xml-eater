package com.martynas.xmleater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlEaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(XmlEaterApplication.class, args);
    }

}
