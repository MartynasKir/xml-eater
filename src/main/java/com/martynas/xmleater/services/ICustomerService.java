package com.martynas.xmleater.services;

import com.martynas.xmleater.model.Customer;
import com.martynas.xmleater.model.CustomerCollection;
import java.util.List;

public interface ICustomerService {

    List<Customer> findAll();

    Customer save(Customer customer);

    void processCustomers(CustomerCollection customerCollection);

}
