package com.martynas.xmleater.services.impl;

import static org.slf4j.LoggerFactory.getLogger;

import com.martynas.xmleater.model.Customer;
import com.martynas.xmleater.model.CustomerCollection;
import com.martynas.xmleater.repository.CustomerRepository;
import com.martynas.xmleater.services.ICustomerService;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerService implements ICustomerService {

    private static final Logger LOG = getLogger(CustomerService.class);
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public void processCustomers(CustomerCollection customerCollection) {
        LOG.debug("CustomerCollection content: {}", customerCollection.getCustomers());
        customerCollection.getCustomers()
                .forEach(this::save);
    }
}
