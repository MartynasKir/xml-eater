package com.martynas.xmleater.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "customers")
@JacksonXmlRootElement(localName = "customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JacksonXmlProperty(isAttribute = true)
    private Long id;

    @JacksonXmlProperty
    private String name;

    @JacksonXmlProperty
    private Long accountNumber;

    @JacksonXmlProperty
    private double balance;

    @JacksonXmlProperty
    private double loyaltyRating;

    public Customer() {
    }

    public Customer(Long id, String name, Long accountNumber, double balance, double loyaltyRating) {
        this.id = id;
        this.name = name;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.loyaltyRating = loyaltyRating;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public double getLoyaltyRating() {
        return loyaltyRating;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id
                + ", name=" + name
                + ", account number=" + accountNumber
                + ", balance=" + balance
                + ", loyalty rating=" + loyaltyRating + "}";
    }
}
