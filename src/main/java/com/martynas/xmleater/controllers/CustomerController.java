package com.martynas.xmleater.controllers;

import static org.slf4j.LoggerFactory.getLogger;

import com.martynas.xmleater.model.Customer;
import com.martynas.xmleater.model.CustomerCollection;
import com.martynas.xmleater.services.ICustomerService;
import java.util.List;
import org.slf4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {

    private static final Logger LOG = getLogger(CustomerController.class);
    private final ICustomerService customerService;

    public CustomerController(ICustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(value = "/customers/upload", consumes = MediaType.APPLICATION_XML_VALUE)
    public void processCustomers(@RequestBody CustomerCollection customerCollection) {
        LOG.debug("Customer Collection: {} ", customerCollection.getCustomers());
        customerService.processCustomers(customerCollection);
    }

    @GetMapping(value = "/customers/get", produces = MediaType.APPLICATION_XML_VALUE)
    public List<Customer> getCustomers() {
        return customerService.findAll();
    }

    @PostMapping(value = "/customer/upload", consumes = MediaType.APPLICATION_XML_VALUE)
    public Customer processCustomers(@RequestBody Customer customer) {
        LOG.debug("Customer : {} ", customer.toString());
        return customerService.save(customer);
    }

}
