package com.martynas.xmleater.services;


import static org.junit.jupiter.api.Assertions.assertEquals;

import com.martynas.xmleater.model.Customer;
import com.martynas.xmleater.model.CustomerCollection;
import com.martynas.xmleater.repository.CustomerRepository;
import com.martynas.xmleater.services.impl.CustomerService;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class CustomerServiceIntegrationTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    @Test
    public void givenCustomerCollection_saveAndRetrieveCustomers() {

        CustomerCollection customerCollection = getCustomerCollection();
        customerService.processCustomers(customerCollection);

        List<Customer> insertedCustomerList = customerCollection.getCustomers();
        List<Customer> retrievedCustomersList = customerRepository.findAll();

        assertEquals(insertedCustomerList.get(0).getAccountNumber(), retrievedCustomersList.get(0).getAccountNumber());
        assertEquals(insertedCustomerList.get(1).getAccountNumber(), retrievedCustomersList.get(1).getAccountNumber());
        assertEquals(insertedCustomerList.get(2).getAccountNumber(), retrievedCustomersList.get(2).getAccountNumber());
    }

    private CustomerCollection getCustomerCollection() {
        CustomerCollection customerCollection = new CustomerCollection();
        customerCollection.setCustomers(Arrays.asList(
                new Customer(1L, "John", 1321312524L, 25.25, 9.6),
                new Customer(2L, "Tom", 13214152442L, 7525.72, 24.2),
                new Customer(3L, "Ben", 13225452114L, 255.45, 17.1)
        ));
        return customerCollection;
    }
}
