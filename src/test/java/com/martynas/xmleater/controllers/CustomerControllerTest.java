package com.martynas.xmleater.controllers;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.martynas.xmleater.services.ICustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ICustomerService customerService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void whenNoXmlGiven_customersEndpointReturnBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/customers/upload")
                .contentType(MediaType.APPLICATION_XML_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenBadXmlGiven_customersEndpointReturnBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/customers/upload")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .content(" <badXml>\n"
                        + "    <badContent incorrect</badContent>\n"
                        + "  </badXml>"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenGoodXmlGiven_customersControllerReturnStatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/customers/upload")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .content(" <customers>\n"
                        + "  <customer>\n"
                        + "    <accountNumber>79158</accountNumber>\n"
                        + "    <balance>709.00</balance>\n"
                        + "    <loyaltyRating>17.4</loyaltyRating>\n"
                        + "    <name>John</name>\n"
                        + "  </customer>\n"
                        + "<customer>\n"
                        + "    <accountNumber>79244</accountNumber>\n"
                        + "    <balance>24.5</balance>\n"
                        + "    <loyaltyRating>4.4</loyaltyRating>\n"
                        + "    <name>Allen</name>\n"
                        + "  </customer>"
                        + "</customers>"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGoodXmlGiven_customerControllerReturnStatusOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/customer/upload")
                .contentType(MediaType.APPLICATION_XML_VALUE)
                .content(" <customer>\n"
                        + "    <accountNumber>79158</accountNumber>\n"
                        + "    <balance>709.00</balance>\n"
                        + "    <loyaltyRating>17.4</loyaltyRating>\n"
                        + "    <name>John</name>\n"
                        + "  </customer>"))
                .andExpect(status().isOk());
    }

}

